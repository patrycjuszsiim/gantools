# client functions for interacting with the ganbreeder api
import requests
import json
import numpy as np

def login(username, password):
    def get_sid():
        url = 'https://artbreeder.com/login'
        r = requests.get(url)
        r.raise_for_status()
        for c in r.cookies:
            if c.name == 'connect.sid': # find the right cookie
                print('Session ID: ' + str(c.value))
                return c.value

    def login_auth(sid, username, password):
        url = 'https://artbreeder.com/login'
        headers = {
                'Content-Type': 'application/json',
                }
        cookies = {
                'connect.sid': sid
                }
        payload = {
                'email': username,
                'password': password
                }
        r = requests.post(url, headers=headers, cookies=cookies, data=json.dumps(payload))
        if not r.ok:
            print('Authentication failed')
            r.raise_for_status()
        print('Authenticated')

    sid = get_sid()
    login_auth(sid, username, password)
    return sid

def parse_info_dict(info):
    keyframe = dict()
    keyframe['truncation'] = np.float(info['truncation'])
    keyframe['latent'] = np.asarray(info['latent'])
    classes = info['classes']
    keyframe['label'] = np.zeros(1000)# length of label ("classes") vector: 1000
    for c in info['classes']:
        # artbreeder class entries look like [index, value] where index < 1000
        keyframe['label'][c[0]] = c[1]
    return keyframe

def get_info(sid, key):
    if sid == '':
        raise Exception('Cannot get info; session ID not defined. Be sure to login() first.')
    cookies = {
            'connect.sid': sid
            }
    url = 'http://artbreeder.com/info?k='+str(key)
    r = requests.get(url, cookies=cookies)
    r.raise_for_status()

    r = r.json()
    if r is not None:
        try:
            return parse_info_dict(r)
        except Exception as err:
            print("Could not retrieve info for key \'{0}\': {1}".format(key, err))
            raise
    else:
        raise RuntimeError("No info could be found at URL \'{0}\'".format(url))

def get_info_batch(username, password, keys):
    l = list()
    sid = login(username, password)
    for key in keys:
        try:
            l.append(get_info(sid, key))
        except Exception as err:
            print("Encountered an error while attempting"
                  "to download info for key {0}".format(key))
            print("Error says: {}".format(err))
            print("Skipping...")
    return l
